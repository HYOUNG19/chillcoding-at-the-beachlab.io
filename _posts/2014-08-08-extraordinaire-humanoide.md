---
title: "L'extraordinaire Humanoïde !"
categories: fr chillcoding android
author: macha
---

<div class="text-center lead" markdown="1">
  ![Android](/assets/img/post/android.png)
</div>

Qui n'a pas déjà entendu parler d'_Android_ ? C'est le fameux robot vert présent
sur les écrans de nos téléphones.

Il a été envoyé par _Google_ pour contrer la planète _Apple_, dont l'armée est
constituée de régiments d'_iPhone_ et d'_iPad_. Il s'est implanté sur quelques
planètes, citons parmi les plus populaires : _Samsung_, _HTC_, et _Sony_. 

En tête des champs de bataille, il y a le régiment de _smartphone_ puis de tablette tactile
mais aussi de table tactile accessible aux grand publics, de TV, de tableaux de bord
pour voiture, pour maison, ou pour réfrigérateur.

Cet incroyable bonhomme vert a pu s'installer sur plusieurs planètes grâce à son
squelette adaptable à n'importe quel robot des divers régiments.

<!--more-->

## Que trouve-t-on sous le capot de ce bonhomme vert ?

Android est un système d'exploitation pour support mobile, soit _Operating System Mobile_ (_OS Mobile_). Il a été développé par une start-up du même nom, racheté par Google en août 2005. Il est basé sur un noyaux _Linux_ allégé, ceci afin de répondre aux contraintes liées aux appareils mobiles (batterie, capacité mémoire, capacité traitement,...). 

La nature open source de _Linux_ et la facilité à reprendre son code source a permis l'utilisation d'_Android_ sur divers supports (du smartphone au tableau de bord en passant par les tables tactiles) de différentes marques (_HTC_, _Samsung_, etc.); et par conséquent cela a favorisé son succès sur le marché des smartphones.

## Comment choisir son environnement de développement ?

Les 3 langages, _kotlin_, _java_ ou _C_, peuvent être utilisés pour programmer sur un appareil _Android_. Cela dit, dans les supports de cours proposés, seul le langage **_kotlin_** est envisagé. 

Un environnement de développement _Android_ est souvent constitué du SDK Android (_System Development Kit_) intégré à un logiciel de développement comme _Eclipse_ ou _NetBean_ (appelé communément _IDE_ pour _Integrated Development Environment_). 

Une formule complète est fournie par _Google_, basé sur _IntelliJ_, sur le site officiel _developer.android.com_. Cette solution est préconisée pour l'installation d'un environnement de développement _Android_ (un clic=un .exe ou un .dmg). 

Note : Une alternative est d'ajouter le plugin _Android_ à un _Eclipse_ contenant des projets existants. 

Ces éléments sont approfondis dans la première séance.

## Comment se former au développement d'une Application Native Android avec le langage Kotlin ?

{% include aside.html %}

### {% icon fa-globe %} Références

* [ChillCoding.com: Installer un environnement de développement Android](https://www.chillcoding.com/blog/2016/08/03/android-studio-installation/)
* [Developer.android.com: Platform Architecture](https://developer.android.com/guide/platform/)
* [Kantar WorldPanel : Part de marché des OS Mobile](www.kantarworldpanel.com/fr/smartphone-os-market-share/)

