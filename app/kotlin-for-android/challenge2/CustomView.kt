package com.chillcoding.magic

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View

class CustomView : View, SensorEventListener {

    lateinit var mCircle: MagicCircle
    private lateinit var mPaint: Paint

    var dX = 0F
    var dY = 0F

    private val circleArray by lazy { Array<MagicCircle>(7) { MagicCircle(width, height) } }

    constructor(context: Context?) : this(context, null)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    private fun init() {
        mPaint = Paint().apply {
            style = Paint.Style.STROKE
            strokeWidth = 10F
        }
        val sensorManager: SensorManager =
            context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
        val accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        if (accelerometer != null) {
            sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_UI)
        }
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        mCircle = MagicCircle(width, height)
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        for (circle in circleArray) {
            with(circle) {
                mPaint.color = mColor
                canvas?.drawCircle(cx, cy, rad, mPaint)
                move()
            }
        }

        mPaint.color = Color.BLUE
        with(mCircle) {
            canvas?.drawCircle(cx, cy, rad, mPaint)
            mCircle.move(dX, dY)
        }
        invalidate()
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (event != null) {
            circleArray[0].moveTo(event.x, event.y)
        }
        return true
    }

    override fun onSensorChanged(event: SensorEvent?) {
        if (event?.sensor?.type == Sensor.TYPE_ACCELEROMETER) {
            dX = event.values[0]
            dY = event.values[1]
        }
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
    }
}