package com.chillcoding.magic

class RainbowCircle(maxX: Int, maxY: Int, x: Float, y: Float) : MagicCircle(maxX, maxY) {
    override var cx = x
    override var cy = y
    override val mColor = App.rainBowColors[App.rainBowColors.indices.random()]
}